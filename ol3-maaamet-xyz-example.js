var mapExtent = [40500, 5993000, 1064500, 7017000];
var mapMinZoom = 4;
var mapMaxZoom = 13;
var mapMaxResolution = 1.000000;
var tileExtent = [40500, 5993000, 1064500, 7017000];
// Proj4js definition (verify code at http://epsg.io/3301);

var mapResolutions = [4000, 2000, 1000, 500, 250, 125, 62.5, 31.25, 15.625, 7.8125, 3.90625, 1.953125, 0.9765625, 0.48828125];


var mapTileGrid = new ol.tilegrid.TileGrid({
  extent: tileExtent,
  minZoom: mapMinZoom,
  resolutions: mapResolutions
});


var map = new ol.Map({
  loadTilesWhileAnimating: true,
  loadTilesWhileInteracting: true,
  target: 'map',
  layers: [
    new ol.layer.Tile({
      source: new ol.source.XYZ({
        projection: 'EPSG:3301',
        tileGrid: mapTileGrid,
        url: "http://tiles.maaamet.ee/tm/s/1.0.0/foto/{z}/{x}/{-y}.jpg"
      })
    }),
    new ol.layer.Tile({
      source: new ol.source.XYZ({
        projection: 'EPSG:3301',
        tileGrid: mapTileGrid,
        url: "http://tiles.maaamet.ee/tm/s/1.0.0/hybriid/{z}/{x}/{-y}.png"
      })
    })
  ],
  view: new ol.View({
    projection: ol.proj.get('EPSG:3301'),
    extent: mapExtent,
    maxResolution: mapTileGrid.getResolution(mapMinZoom),
    center: ol.proj.fromLonLat([24.793996, 59.443378]),
  })
});
map.getView().fit(mapExtent, map.getSize());
