import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoDisplacementsMapComponent } from './geodisplacements-map.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MapService } from '../services/map.service';

describe('GeoDisplacementsMapComponent', () => {
  let component: GeoDisplacementsMapComponent;
  let fixture: ComponentFixture<GeoDisplacementsMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GeoDisplacementsMapComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MapService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoDisplacementsMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.map).toBeDefined();
    expect(component.map.getTarget()).toBe('geodisplacements-map');
  });

  it('should set initial values', () => {
    expect(component.filters).toBeTruthy();
    expect(component.filters.selectedBridge).not.toBeDefined();
    expect(component.filters.selectedAnalysis).toBeNull();
    expect(component.filters.selectedLegend).toBeTruthy();

    expect(component.selectedPoint).not.toBeDefined();
    expect(component.msgs.length).toBe(0);
  });

});
