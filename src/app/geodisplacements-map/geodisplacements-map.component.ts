import { Component, OnInit } from '@angular/core';
import { MapService } from '../services/map.service';
import { FeatureFilters } from '../domain/FeatureFilters';
import { AnalysisPoint } from '../domain/AnalysisPoint';
import { Message } from 'primeng/primeng';


@Component({
  selector: 'geodisplacements-map',
  templateUrl: './geodisplacements-map.component.html',
  styleUrls: ['./geodisplacements-map.component.css']
})
export class GeoDisplacementsMapComponent implements OnInit {

  public map: any;

  public filters: FeatureFilters = new FeatureFilters();

  public selectedPoint: AnalysisPoint;

  public sidebarOpen = true;

  public msgs: Message[] = [];

  constructor(private mapService: MapService) {
    this.map = this.mapService.map;
  }

  ngOnInit() {
    this.map.setTarget('geodisplacements-map');
    this.mapService.fitExtent();
    this.mapService.updateMapSize();
  }

  pointSelected(point: AnalysisPoint) {
    this.selectedPoint = point;
    if (this.filters.selectedAnalysis) {
      this.filters.selectedAnalysis.updatePointsActive(point);
    }
  }

}
