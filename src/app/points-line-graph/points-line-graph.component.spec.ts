import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsLineGraphComponent } from './points-line-graph.component';
import { MapService } from '../services/map.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DataService } from '../services/data.service';
import { DataServiceMock } from '../services/test/data.service.mock';
import { GraphMathService } from "../services/graph.math.service";

describe('PointsLineGraphComponent', () => {
  let component: PointsLineGraphComponent;
  let fixture: ComponentFixture<PointsLineGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsLineGraphComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MapService, {provide: DataService, useValue: new DataServiceMock()}, GraphMathService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsLineGraphComponent);
    component = fixture.componentInstance;
    const dataServiceMock = new DataServiceMock();
    dataServiceMock.getAnalysisPoints(0).subscribe(points => component.points = [points[0]]);
    dataServiceMock.getBridgeAnalysis(0).subscribe(analysis => component.points[0].analysis = analysis[0]);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate dataset for graph', () => {
    component.generateGraph();

    expect(component.data).toBeTruthy();
    expect(component.data.labels.length).toBe(10); // 10 different image dates in data
    expect(component.data.datasets.length).toBe(2); // base point + regression line
    expect(component.data.datasets[0].data.length).toBe(10);
  });

});
