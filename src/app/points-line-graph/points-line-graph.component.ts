import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AnalysisPoint } from '../domain/AnalysisPoint';
import { DataService } from '../services/data.service';
import { MapService } from '../services/map.service';
import {
  AVERAGE_LINE_COLOR,
  AVERAGE_VALUE_FRACTION,
  POINT_EVENT_CLICK,
  REGRESSION_LINE_COLOR
} from '../app.constants';
import { AnalysisPointData } from '../domain/AnalysisPointData';

import * as _ from 'lodash';
import { GraphMathService } from '../services/graph.math.service';

@Component({
  selector: 'app-points-line-graph',
  templateUrl: './points-line-graph.component.html',
  styleUrls: ['./points-line-graph.component.css']
})
export class PointsLineGraphComponent implements OnInit {

  dialogOpen = false;
  singlePoint: AnalysisPoint;

  data: any = {
    labels: [],
    datasets: []
  };

  options: any= {
    elements : { line : { tension : 0 } }
  };

  @Input()
  points: AnalysisPoint[];

  @Input()
  label: string;


  @Input()
  get open() {
    return this.dialogOpen;
  }
  set open(val) {
    this.dialogOpen = val;
    this.openChange.emit(this.dialogOpen);
  }

  @Output()
  openChange = new EventEmitter();

  constructor(private dataService: DataService, private  mapService: MapService, private mathService: GraphMathService) {
    this.registerPointClickEvent();
  }

  ngOnInit() {}

  generateGraph(): void {
    const singlePoint = this.singlePoint;
    const pointsCollection = _.isObject(singlePoint) ? [singlePoint] : this.points;
    const totalPoints = pointsCollection.length;

    if (totalPoints === 0) {
      this.data = {};
      return;
    }

    const data = { labels: [], datasets: [] };
    const pointIds: number[] = _.map(pointsCollection, point => point.id);
    this.dataService.getAllPointsDetails(pointIds).subscribe((pointsDataSets: AnalysisPointData[][]) => {

      const [anyDataSet] = pointsDataSets;
      data.labels = _.map(anyDataSet, dataSet => dataSet.imageDate);

      _.each(pointsDataSets, (pointDataSet, pointIndex) => {
        const point = pointsCollection[pointIndex];
        data.datasets.push({
          label: `${point.cumulativeDisplacement}, ${point.coherence}`,
          data: _.map(pointDataSet, setPoint => setPoint.displacement),
          fill: false,
          borderColor: point.generateHSLValue()
        });
      });

      if (totalPoints > 1) {
        data.datasets.push(this.getAverageDataSet(pointsCollection, pointsDataSets));
      }

      data.datasets.push(this.getRegressionDataSet(pointsDataSets));

      this.singlePoint = null;
      this.data = data;
    });
  }

  getRegressionDataSet(dataSets: AnalysisPointData[][]) {
    const { resultSet, formula } = this.mathService.getLinearRegressionDataSet(dataSets);
    return {
      label: `Linear regression: ${formula}`,
      data: resultSet,
      fill: true,
      borderColor: REGRESSION_LINE_COLOR
    };
  }

  getAverageDataSet(points: AnalysisPoint[], dataSets: AnalysisPointData[][]) {
    const meanDataSet = this.mathService.getMeanDataSet(dataSets);
    const displacementAverage = _.meanBy(points, point => point.cumulativeDisplacement)
                                 .toFixed(AVERAGE_VALUE_FRACTION)
                                 .toString();
    const coherenceAverage = _.meanBy(points, point => point.coherence)
                              .toFixed(AVERAGE_VALUE_FRACTION)
                              .toString();
    return {
      label: `(Keskmine) ${displacementAverage}, ${coherenceAverage}`,
      data: meanDataSet,
      fill: false,
      borderColor: AVERAGE_LINE_COLOR
    };
  }

  private registerPointClickEvent() {
    this.mapService.onPointEvent(POINT_EVENT_CLICK, (point) => {
      this.singlePoint = point;
      this.open = true;
    });
  }

}
