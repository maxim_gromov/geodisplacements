import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { AnalysisPoint } from '../domain/AnalysisPoint';

@Component({
  selector: 'app-point-info',
  templateUrl: './point-info.component.html',
  styleUrls: ['./point-info.component.css']
})
export class PointInfoComponent implements OnInit {

  @Input()
  public point: AnalysisPoint;


  constructor() {
  }

  ngOnInit() {
  }


}
