import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointInfoComponent } from './point-info.component';
import { MapService } from '../services/map.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DataService } from '../services/data.service';
import { DataServiceMock } from '../services/test/data.service.mock';
import { ConfirmationService } from 'primeng/primeng';
import { By } from '@angular/platform-browser';

describe('PointInfoComponent', () => {
  let component: PointInfoComponent;
  let fixture: ComponentFixture<PointInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointInfoComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MapService, {provide: DataService, useValue: new DataServiceMock()}, ConfirmationService],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show point info, if point is selected', () => {
    const pointInfo = fixture.debugElement.query(By.css('.point-info'));
    expect(pointInfo).toBeNull();

    component.point = new DataServiceMock().getAnalysisPoints(2)[0];
    fixture.detectChanges();

    const pointInfoAfter = fixture.debugElement.query(By.css('.point-info'));
    expect(pointInfoAfter).toBeDefined();
  });
});
