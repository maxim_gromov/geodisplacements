import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';

import {
  ButtonModule,
  GrowlModule,
  DropdownModule,
  SelectButtonModule,
  DialogModule,
  ChartModule,
  ConfirmDialogModule,
  ConfirmationService
} from 'primeng/primeng';

import { AppComponent } from './app.component';
import { GeoDisplacementsMapComponent } from './geodisplacements-map/geodisplacements-map.component';
import { PointsLegendComponent } from './points-legend/points-legend.component';
import { PointsLineGraphComponent } from './points-line-graph/points-line-graph.component';
import { FiltersAreaComponent } from './filters-area/filters-area.component';
import { PointInfoComponent } from './point-info/point-info.component';
import { PointsToolsComponent } from './points-tools/points-tools.component';
import { SidebarToggleComponent } from './sidebar-toggle/sidebar-toggle.component';

import { DataService } from './services/data.service';
import { MapService } from './services/map.service';
import { GraphMathService } from './services/graph.math.service';

import { MouseWheelDirective } from './directives/mouse.wheel.directive';


@NgModule({
  declarations: [
    // Application components section
    AppComponent,
    GeoDisplacementsMapComponent,
    PointsLegendComponent,
    PointsLineGraphComponent,
    FiltersAreaComponent,
    PointInfoComponent,
    SidebarToggleComponent,
    PointsToolsComponent,

    MouseWheelDirective,
  ],
  imports: [
    // Angular modules
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,

    // External modules
    MaterialModule,
    ConfirmDialogModule,
    GrowlModule,
    ButtonModule,
    DropdownModule,
    SelectButtonModule,
    DialogModule,
    ChartModule,
  ],
  providers: [ConfirmationService, MapService, DataService, GraphMathService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
