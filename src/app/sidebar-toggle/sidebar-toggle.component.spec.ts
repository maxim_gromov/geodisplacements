import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarToggleComponent } from './sidebar-toggle.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MapService } from '../services/map.service';

describe('SidebarToggleComponent', () => {
  let component: SidebarToggleComponent;
  let fixture: ComponentFixture<SidebarToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarToggleComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MapService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarToggleComponent);
    component = fixture.componentInstance;
    component.sidebarOpen = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle sidebar', (done) => {
    component.sidebarOpenChange.subscribe(open => {
      expect(open).toEqual(false);
      done();
    });
    component.toggleSidebar();
  });

});
