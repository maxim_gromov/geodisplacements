import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MapService } from '../services/map.service';

@Component({
  selector: 'app-sidebar-toggle',
  templateUrl: './sidebar-toggle.component.html',
  styleUrls: ['./sidebar-toggle.component.css']
})
export class SidebarToggleComponent implements OnInit {

  @Input()
  public sidebarOpen: boolean;

  @Output() sidebarOpenChange = new EventEmitter();

  constructor(private mapService: MapService) { }

  ngOnInit() {}

  toggleSidebar() {
    this.sidebarOpenChange.emit(!this.sidebarOpen);
    this.mapService.updateMapSize();
  }

}
