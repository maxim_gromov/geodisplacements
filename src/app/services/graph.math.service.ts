import { Injectable } from '@angular/core';
import { AnalysisPointData } from '../domain/AnalysisPointData';

import * as _ from 'lodash';
import * as regression from 'regression';

@Injectable()
export class GraphMathService {

  /* Iterate each X axis point with all points. pI - point index, d - displacement */
  private static iterateDataSets(dataSets: AnalysisPointData[][], callBack: (pI: number, d: number) => void) {
    const totalPoints = dataSets.length;
    const [ anySet ] = dataSets,
      setLength = anySet.length;

    for (let x = 0; x < setLength; x++) {
      for (let y = 0; y < totalPoints; y++) {
        const { displacement } = dataSets[y][x];
        callBack(x, displacement);
      }
    }
  }

  constructor() {}

  public getLinearRegressionDataSet(dataSets: AnalysisPointData[][]): any {
    const resultSet: number[] = [];

    const [ anySet ] = dataSets,
      setLength = anySet.length,
      dataSet = [];

    GraphMathService.iterateDataSets(dataSets, (pointIndex, displacement) => dataSet.push([pointIndex, displacement]));

    const result = regression('linear', dataSet);

    const formula: string = result.string;
    const [ slope, yIntercept ] = result.equation;

    for (let x = 0; x < setLength; x++) {
      resultSet.push(slope * (x + 1) + yIntercept)
    }

    return { resultSet, formula };
  }

  public getMeanDataSet(dataSets: AnalysisPointData[][]): number[] {
    const meanSet = [],
          totalPoints = dataSets.length;
    const tempDisplacements = [];
    GraphMathService.iterateDataSets(dataSets, (pointIndex, displacement) => {
      tempDisplacements.push(displacement);
      if (tempDisplacements.length === totalPoints) {
        meanSet.push(_.mean(tempDisplacements));
        tempDisplacements.length = 0;
      }
    });
    return meanSet;
  }

}
