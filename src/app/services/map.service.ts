import { Injectable } from '@angular/core';

import {
  DRAWN_AVERAGE_AREA_OPACITY,
  DRAWN_AVERAGE_NUMBER_FRACTION,
  ESTONIA_EXTENT,
  ESTONIA_VIEW_EXTENT,
  GEOMETRY_TEXT_STYLE_COLOR,
  GEOMETRY_TEXT_STYLE_FONT,
  MAAAMET_TMS_HYBRID,
  MIN_RESOLUTION_TO_SHOW_TEXT,
  ESTONIA_MIN_ZOOM,
  ESTONIA_RESOLUTIONS,
  ESTONIA_PROJECTION,
  ESTONIA_CENTER,
  MAAAMET_TMS_FOTO,
  GEOMETRY_TEXT_STYLE_BORDER_COLOR,
  POINT_EVENT_HOVER,
  POINT_POPUP_OFFSET_XY,
} from '../app.constants';

import { AnalysisPoint } from '../domain/AnalysisPoint';

import {
  tilegrid,
  layer,
  source,
  proj,
  interaction,
  events,
  extent,
  geom,
  style,
  View,
  Map,
  Feature,
  Overlay,
} from 'openlayers';

import proj4 from 'proj4';
import * as _ from 'lodash';
import { FeatureType } from '../domain/FeatureType';

// Proj4js definition (verify code at http://epsg.io/3301);
proj.setProj4(proj4);
proj4.defs(ESTONIA_PROJECTION, '+proj=lcc +lat_1=59.33333333333334 +lat_2=58 +lat_0=57.51755393055556 +lon_0=24 +x_0=500000 +y_0=6375000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');

@Injectable()
export class MapService {

  map: Map;
  vectors: source.Vector[] = [];

  drawInteraction: interaction.Draw;
  drawnFeatures: Feature[] = [];

  pointPopup: Overlay;

  constructor() {
    this.map = this.createMap();
  }

  public createPointPopup(): void {
    this.pointPopup = new Overlay({
      offset: POINT_POPUP_OFFSET_XY,
      element: document.getElementById('point-popup')
    });
    this.map.addOverlay(this.pointPopup);
  }

  public updateMapSize(): void {
    setTimeout(() => this.map.updateSize(), 400);
  }

  public addFeature(featureType: FeatureType, feature: Feature[] | Feature): void {
    const features = _.isArray(feature) ? feature : [feature];
    this.vectors[featureType].addFeatures(features);
  }

  public clearAllFeatures(): void {
    this.vectors.forEach(vector => vector.clear());
  }

  public clearDrawnFeatures(): void {
    this.vectors[FeatureType.DRAW].clear();
    this.vectors[FeatureType.DRAW_AVERAGE_TEXT].clear();
  }

  public fitExtent(extent?: number[], zoom?: number): void {
    const extentToFit = extent ? extent : ESTONIA_EXTENT;
    this.map.getView().fit(extentToFit, this.map.getSize());
    if (zoom) {
      this.map.getView().setZoom(zoom);
    }
  }

  public focusGeometry(geometry: geom.Geometry): void {
    this.map.getView().setCenter(geometry.getCoordinates());
  }

  public forEachPointThat(predicate: (p?: AnalysisPoint, f?: Feature) => boolean,
                          action: (p?: AnalysisPoint, f?: Feature) => void): void {
    _.each(this.vectors[FeatureType.POINT].getFeatures(), (feature) => {
        const point = feature.get('pointReference');
        if (predicate(point, feature)) {
          action(point, feature);
        }
    });
  }

  public extractFilteredPointsFromDrawnFeature(drawnFeature: Feature): AnalysisPoint[] {
    const drawnFeaturePoints = [];
    const drawnFeatureGeometry = drawnFeature.getGeometry();
    const drawnFeatureStyleFunction = this.getDrawnFeatureStyleFunction(drawnFeaturePoints);

    drawnFeature.setStyle(drawnFeatureStyleFunction);

    this.forEachPointThat(
      (point, feature) => drawnFeatureGeometry.intersectsCoordinate(feature.getGeometry().getCoordinates()),
      (point) => drawnFeaturePoints.push(point)
    );
    return drawnFeaturePoints;
  }

  public clearPoints(): void {
    this.vectors[FeatureType.POINT].clear();
  }

  public onPointEvent(eventType: string, doAction: (p: AnalysisPoint, c?: any[]) => void): void {
    this.map.on(eventType, (event) => {
      const feature = this.map.forEachFeatureAtPixel(event.pixel, (someFeature, layer) => someFeature);
      if (feature) {
        const point = feature.get('pointReference');
        if (point || eventType === POINT_EVENT_HOVER) {
          doAction(point, event.coordinate);
        }
      }
    });
  }

  public updateLegendFeatures(): void {
    _.each(this.vectors[FeatureType.POINT].getFeatures(), feature => feature.changed());
    _.each(this.vectors[FeatureType.DRAW_AVERAGE_TEXT].getFeatures(), feature => feature.changed());
    _.each(this.vectors[FeatureType.DRAW].getFeatures(), feature => feature.changed());
  }

  public toggleDrawMode(enable: boolean): void {
    if (enable) {
      this.drawInteraction = new interaction.Draw({
        source: this.vectors[FeatureType.DRAW],
        type: 'Polygon',
        freehand: true,
        features: this.drawnFeatures,
      });
      this.map.addInteraction(this.drawInteraction);
    } else if (this.drawInteraction != null) {
      this.map.removeInteraction(this.drawInteraction);
    }
  }

  public addDrawingAverageText(points: AnalysisPoint[], originalGeometry: geom.Geometry): void {
    const averageFeature = new Feature({
      geometry: originalGeometry
    });

    averageFeature.setStyle((feature, resolution) => {
      let pointsInsideFeature = feature.get('points');

      if (!pointsInsideFeature) {
        feature.set('points', points);
        pointsInsideFeature = points;
      }

      const [firstPoint] = pointsInsideFeature;
      const legend = firstPoint.analysis.legend;
      const average = _.meanBy(points, (p) => p[legend.value]);

      return new style.Style({
        text: new style.Text({
          font: GEOMETRY_TEXT_STYLE_FONT,
          fill: new style.Fill({
            color: GEOMETRY_TEXT_STYLE_COLOR
          }),
          stroke: new style.Stroke({
            color: GEOMETRY_TEXT_STYLE_BORDER_COLOR,
            width: 1
          }),
          text: resolution < MIN_RESOLUTION_TO_SHOW_TEXT
            ? average.toFixed(DRAWN_AVERAGE_NUMBER_FRACTION).toString()
            : ''
        })
      });
    });

    this.addFeature(FeatureType.DRAW_AVERAGE_TEXT, averageFeature);
  }

  private getDrawnFeatureStyleFunction(points: any[]) {
    return (feature, resolution) => {
      let pointsInsideFeature = feature.get('points');

      if (!pointsInsideFeature) {
        feature.set('points', points);
        pointsInsideFeature = points;
      }

      const styles = [];
      const notEmpty = pointsInsideFeature.length > 0;
      if (notEmpty) {
        const [firstPoint] = pointsInsideFeature;
        const legend = firstPoint.analysis.legend;
        const average = _.meanBy(pointsInsideFeature, (p) => p[legend.value]);
        const averageColor = firstPoint.generateHSLValue(average, DRAWN_AVERAGE_AREA_OPACITY);

        const averageFill = new style.Style({
          fill: new style.Fill({
            color: averageColor
          }),
        });
        styles.push(averageFill);
      }
      return styles;
    };
  }

  private createMap(): Map {
    return new Map({
      loadTilesWhileAnimating: true,
      loadTilesWhileInteracting: true,
      layers: [
        this.createFotoTileLayer(),
        this.createHybridTileLayer(),

        this.createLayer(FeatureType.BRIDGE),
        this.createLayer(FeatureType.DRAW),
        this.createLayer(FeatureType.ARTIFICIAL_SCATTERER),
        this.createLayer(FeatureType.POINT),
        this.createLayer(FeatureType.DRAW_AVERAGE_TEXT),
      ],
      view: this.getView()
    });
  }

  private getTileGrid(): tilegrid.TileGrid {
    return new tilegrid.TileGrid({
      extent: ESTONIA_EXTENT,
      minZoom: ESTONIA_MIN_ZOOM,
      resolutions: ESTONIA_RESOLUTIONS,
    });
  }

  private createHybridTileLayer(): layer.Tile {
    return new layer.Tile({
      source: new source.XYZ({
        projection: ESTONIA_PROJECTION,
        tileGrid: this.getTileGrid(),
        url: MAAAMET_TMS_HYBRID
      })
    });
  }

  private createFotoTileLayer(): layer.Tile {
    return new layer.Tile({
      source: new source.XYZ({
        projection: ESTONIA_PROJECTION,
        tileGrid: this.getTileGrid(),
        url: MAAAMET_TMS_FOTO
      })
    })
  }

  private createLayer(featureType: FeatureType): source.Vector {
    const layerSourceVector = new source.Vector();
    this.vectors[featureType] = layerSourceVector;
    return new layer.Vector({
      source: layerSourceVector
    });
  }

  private getView(): View {
    return new View({
      projection: proj.get(ESTONIA_PROJECTION),
      extent: ESTONIA_VIEW_EXTENT,
      maxResolution: this.getTileGrid().getResolution(ESTONIA_MIN_ZOOM),
      center: ESTONIA_CENTER
    });
  }

}
