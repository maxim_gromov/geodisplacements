import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs';
import { Bridge } from '../domain/Bridge';
import { BridgeAnalysis } from '../domain/BridgeAnalysis';
import { AnalysisPoint } from '../domain/AnalysisPoint';
import { AnalysisPointData } from '../domain/AnalysisPointData';
import * as _ from 'lodash';
import {
  BRIDGE_API_ENDPOINT, ENABLE_API, BRIDGE_ANALYSIS_API_ENDPOINT, BRIDGE_ANALYSIS_POINTS_API_ENDPOINT,
  BRIDGE_ANALYSIS_POINTS_DETAILS_ENDPOINT
} from '../app.constants';

@Injectable()
export class DataService {

  constructor(private http: Http) { }

  getBridges(): Observable<Bridge[]> {
    const endpoint = ENABLE_API ? BRIDGE_API_ENDPOINT : './assets/data/analysis.json';
    return this.http.get(endpoint)
               .map(response => this.toTypedArray(response.json(), Bridge));
  }

  getBridgeAnalysis(bridgeId: number): Observable<BridgeAnalysis[]> {
    const endpoint = ENABLE_API ? `${BRIDGE_ANALYSIS_API_ENDPOINT}${bridgeId}` : './assets/data/analysis_object.json';
    return this.http.get(endpoint)
               .map(response => this.toTypedArray(response.json().analyzes, BridgeAnalysis));
  }

  getAnalysisPoints(analysisId: number): Observable<AnalysisPoint[]> {
    const endpoint = ENABLE_API ? `${BRIDGE_ANALYSIS_POINTS_API_ENDPOINT}${analysisId}` : './assets/data/analysis_points.json';
    return this.http.get(endpoint)
               .map(response => this.toTypedArray(response.json(), AnalysisPoint));
  }

  getPointDetails(pointId: number): Observable<AnalysisPointData[]> {
    const endpoint = ENABLE_API ? `${BRIDGE_ANALYSIS_POINTS_DETAILS_ENDPOINT}${pointId}` : `../assets/data/point_${pointId}_details.json`;
    return this.http.get(endpoint)
               .map(response => this.toTypedArray(response.json(), AnalysisPointData));
  }

  getAllPointsDetails(pointIds: number[]): Observable<AnalysisPointData[][]> {
    const requests = _.map(pointIds, pointId => this.getPointDetails(pointId));
    return Observable.forkJoin(requests);
  }

  toTypedArray<T>(fromItems: any[], Type: { new(...args: any[]): T; }): T[] {
    return _.map(fromItems, item => new Type(item));
  }

}
