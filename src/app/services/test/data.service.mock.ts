/* Filters service Mock class */
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { Bridge } from '../../domain/Bridge';
import { BridgeAnalysis } from '../../domain/BridgeAnalysis';
import { AnalysisPoint } from '../../domain/AnalysisPoint';
import { AnalysisPointData } from '../../domain/AnalysisPointData';

export class DataServiceMock extends DataService {

  testDataSet = [
    {
      "id": 1,
      "displacement": 2.0,
      "imageDate": "2011-01-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 2,
      "displacement": 4.0,
      "imageDate": "2012-01-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 3,
      "displacement": 3.0,
      "imageDate": "2012-02-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 4,
      "displacement": 6.0,
      "imageDate": "2012-03-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 5,
      "displacement": 1.0,
      "imageDate": "2012-04-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 6,
      "displacement": -2.0,
      "imageDate": "2012-05-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 7,
      "displacement": -3.0,
      "imageDate": "2012-06-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus:10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 8,
      "displacement": 3.0,
      "imageDate": "2012-07-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 9,
      "displacement": 5.0,
      "imageDate": "2012-08-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    },
    {
      "id": 10,
      "displacement": 4.0,
      "imageDate": "2012-09-01",
      "outputPoint": {
        "id": 7,
        "coherence": 0.92,
        "cumulativeDisplacement": 10.0,
        "height": 4.0,
        "heightWrt": 4.0,
        "location": {
          "type": "Point",
          "coordinates": [
            24.272,
            58.3972
          ]
        },
        "sigmaHeight": 1.0,
        "sigmaVelocity": 1.0,
        "velocity": 1.0,
        "pointName": "Kumulatiivne muutus: 10,00, koherentsus: 0,92"
      }
    }
  ];

  constructor() {
    super(null);
  }

  getBridges() {
    console.log('Returning mocked test data for bridges.');
    return Observable.of(this.toTypedArray([
      {
        id: 1,
        name: 'Sild 1',
        location: {
          type: 'Polygon',
          coordinates: [
            [
              [
                24.271714029,
                58.3972085
              ],
              [
                24.271896419,
                58.3969387
              ],
              [
                24.274203119,
                58.3973772
              ],
              [
                24.273988542,
                58.3975964
              ],
              [
                24.271714029,
                58.3972085
              ]
            ]
          ]
        },
        artificialScatterers: []
      },
      {
        id: 2,
        name: 'Sild 2',
        location: {
          type: 'Polygon',
          coordinates: [
            [
              [
                24.271714029,
                58.3972085
              ],
              [
                24.271896419,
                58.3969387
              ],
              [
                24.274203119,
                58.3973772
              ],
              [
                24.273988542,
                58.3975964
              ],
              [
                24.271714029,
                58.3972085
              ]
            ]
          ]
        },
        artificialScatterers: []
      }
    ], Bridge));
  }

  getBridgeAnalysis(bridgeId: number) {
    console.log('Returning mocked test data for bridge analysis.');
    return Observable.of(this.toTypedArray([
      {
        id: 1,
        analysisDate: '2016-12-25T00:00:00',
        firstImageDate: '2015-01-01T00:00:00',
        lastImageDate: '2017-01-01T00:00:00',
      },
      {
        id: 2,
        analysisDate: '2012-12-25T00:00:00',
        firstImageDate: '2010-01-01T00:00:00',
        lastImageDate: '2013-01-01T00:00:00',
      }
    ], BridgeAnalysis));
  }

  getAnalysisPoints(analysisId: number) {
    console.log('Returning mocked test data for analysis points.');
    return Observable.of(this.toTypedArray([
      {
        id: 4,
        coherence: 0.92,
        cumulativeDisplacement: 0.0,
        height: 4.0,
        heightWrt: 4.0,
        location: {
          type: 'Point',
          coordinates: [
            24.980563,
            59.454920
          ]
        },
        sigmaHeight: 1.0,
        sigmaVelocity: 1.0,
        velocity: -4.0
      },
      {
        id: 3,
        coherence: 0.92,
        cumulativeDisplacement: -7.0,
        height: 4.0,
        heightWrt: 4.0,
        location: {
          type: 'Point',
          coordinates: [
            24.980797,
            59.455081
          ]
        },
        sigmaHeight: 1.0,
        sigmaVelocity: 1.0,
        velocity: -5.0
      }
    ], AnalysisPoint));
  }

  getPointDetails(pointId: number): Observable<AnalysisPointData[]> {
    return Observable.of(this.toTypedArray(this.testDataSet, AnalysisPointData));
  }

  getAllPointsDetails(pointIds: number[]): Observable<AnalysisPointData[][]> {
    return Observable.of([this.toTypedArray(this.testDataSet, AnalysisPointData)]);
  }

}
