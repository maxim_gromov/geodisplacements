import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsToolsComponent } from './points-tools.component';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { DataServiceMock } from "../services/test/data.service.mock";
import { DataService } from "../services/data.service";
import { MapService } from "../services/map.service";
import { ConfirmationService } from "primeng/primeng";
import { TOOLTIP_DRAW_ACTIVATED, TOOLTIP_DRAW_DISACTIVATED } from '../app.constants';

describe('PointsToolsComponent', () => {
  let component: PointsToolsComponent;
  let fixture: ComponentFixture<PointsToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsToolsComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MapService, {provide: DataService, useValue: new DataServiceMock()}, ConfirmationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsToolsComponent);
    component = fixture.componentInstance;
    const dataServiceMock = new DataServiceMock();
    dataServiceMock.getBridgeAnalysis(0).subscribe(analysis => component.analysis = analysis[0]);
    dataServiceMock.getBridges().subscribe(bridges => component.analysis.bridge = bridges[0]);
    dataServiceMock.getAnalysisPoints(0).subscribe(points => component.analysis.points = points);
    component.msgs = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should notify subscribers, when focused to the bridge', (done) => {
    component.pointChange.subscribe(point => {
      expect(point).toBeNull();
      done();
    });
    component.focusBridge();
  });

  it('should toggle draw mode (activate)', () => {
    component.toggleDrawMode();

    expect(component.drawMode).toBeTruthy();
    expect(component.msgs.length).toBe(1);
    expect(component.msgs[0].detail).toBe(TOOLTIP_DRAW_ACTIVATED);
  });

  it('should toggle draw mode (disactivate)', () => {
    component.toggleDrawMode();
    component.toggleDrawMode();

    expect(component.drawMode).toBeFalsy();
    expect(component.msgs.length).toBe(2);
    expect(component.msgs[1].detail).toBe(TOOLTIP_DRAW_DISACTIVATED);
  });
});
