import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { AnalysisPoint } from '../domain/AnalysisPoint';
import { BridgeAnalysis } from '../domain/BridgeAnalysis';
import { ConfirmationService, Message } from 'primeng/primeng';
import { DataService } from '../services/data.service';
import { MapService } from '../services/map.service';
import { DRAW_END_EVENT, POINT_EVENT_CLICK, TOOLTIP_DRAW_ACTIVATED, TOOLTIP_DRAW_DISACTIVATED } from '../app.constants';

import * as json2csv from 'json2csv';
import * as _ from 'lodash';

@Component({
  selector: 'app-points-tools',
  templateUrl: './points-tools.component.html',
  styleUrls: ['./points-tools.component.css']
})
export class PointsToolsComponent implements OnInit, OnChanges {

  dialogOpen = false;
  drawMode = false;
  filteredPoints: AnalysisPoint[] = [];

  @Input()
  msgs: Message[];

  @Input()
  public analysis: BridgeAnalysis;

  @Output()
  public pointChange = new EventEmitter();

  private static extractCSVObjectFromPoint(point: AnalysisPoint): any {
    const {id, cumulativeDisplacement, velocity, coherence, height, heightWrt, sigmaHeight, sigmaVelocity} = point;
    const [lon, lat] = point.location.coordinates;
    return {
      'ID': id,
      'LAT': lat,
      'LON': lon,
      'HEIGHT': height,
      'HEIGHT WRT DEM': heightWrt,
      'SIGMA HEIGHT': sigmaHeight,
      'VEL': velocity,
      'SIGMA VEL': sigmaVelocity,
      'CUM DISP': cumulativeDisplacement,
      'COHER': coherence
    };
  }

  private static downloadAsCSV(csv: string, fileName: string): void {
    const adjustedFileName = `${fileName.replace(/ /g, '_')}.csv`;
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(new Blob([csv], {'type': 'text/csv;charset=utf8;'}), adjustedFileName);
    } else {
      const link = document.createElement('a');
      link.href = `data:text/csv;charset=utf-8,${encodeURI(csv)}`;
      link.setAttribute('visibility', 'hidden');
      link.download = adjustedFileName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  constructor(private mapService: MapService,
              private dataService: DataService,
              private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.registerPointClickEvent();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { analysis } = changes;
    if (_.isObject(analysis)) {
      const { previousValue, currentValue } = analysis;
      if (!_.isEqual(previousValue, currentValue)) {
        this.filteredPoints = [];
      }
    }
  }

  focusBridge() {
    this.pointChange.emit(null);
    this.mapService.fitExtent(this.analysis.bridge.geometry.getExtent());
  }

  toggleDrawMode() {
    const message = this.drawMode ? TOOLTIP_DRAW_DISACTIVATED : TOOLTIP_DRAW_ACTIVATED;
    if (this.drawMode && this.filteredPoints.length > 0) {
      this.showDrawingsClearConfirmDialog();
    }

    this.drawMode = !this.drawMode;
    this.mapService.toggleDrawMode(this.drawMode);
    this.registerDrawEndEvent();
    this.msgs.push({severity: 'info', summary: 'Abi', detail: message});

  }

  exportCsv() {
    const fileName = `${this.analysis.bridge.name}.${this.analysis.label}`;
    const points = this.analysis.points;
    const pointIds = _.map(points, point => point.id);
    const data = [];

    _.each(points, point => data.push(PointsToolsComponent.extractCSVObjectFromPoint(point)));

    this.dataService.getAllPointsDetails(pointIds).subscribe(pointsDatas => {
      _.each(pointsDatas, (pointDatas, index) => {
        _.each(pointDatas, pointData => data[index][pointData.imageDate] = pointData.displacement);
      });
      const convertedJSON = json2csv({data: data});
      PointsToolsComponent.downloadAsCSV(convertedJSON, fileName);
    });
  }

  selectPoint(point: AnalysisPoint) {
    this.pointChange.emit(point);
    this.mapService.focusGeometry(point.geometry);
  }

  private showDrawingsClearConfirmDialog(): void {
    this.confirmationService.confirm({
      message: 'Kas kustutan kujutatud joonistused?',
      accept: () => {
        this.mapService.clearDrawnFeatures();
        this.filteredPoints = [];
      }
    });
  }

  private registerDrawEndEvent(): void {
    this.mapService.drawInteraction.on(DRAW_END_EVENT, (event) => {
      const pointsInsideFeature = this.mapService.extractFilteredPointsFromDrawnFeature(event.feature);
      _.each(pointsInsideFeature, point => {
        if (this.filteredPoints.indexOf(point) === -1) {
          this.filteredPoints.push(point);
        }
      });

      if (pointsInsideFeature.length > 0) {
        this.mapService.addDrawingAverageText(pointsInsideFeature, event.feature.getGeometry());
        this.msgs.push({severity: 'info', summary: 'Abi', detail: `Valikus on ${this.filteredPoints.length} punkti.`});
      }
    });
  }

  private registerPointClickEvent(): void {
    this.mapService.onPointEvent(POINT_EVENT_CLICK, point => this.selectPoint(point));
  }

}
