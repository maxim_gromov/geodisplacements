export const DEFAULT_DATE_FORMAT = 'dd.MM.yyyy';
export const DEFAULT_LOCALE = 'en-US';

/* API */
export const ENABLE_API = false;

export const API_ENDPOINT = '>unset<';

export const BRIDGE_API_ENDPOINT = `${API_ENDPOINT}/analysis_objects`;
export const BRIDGE_ANALYSIS_API_ENDPOINT = `${API_ENDPOINT}/analysis_object/`;
export const BRIDGE_ANALYSIS_POINTS_API_ENDPOINT = `${API_ENDPOINT}/points/`;
export const BRIDGE_ANALYSIS_POINTS_DETAILS_ENDPOINT = `${API_ENDPOINT}/point/`;

// Source - http://www.maaamet.ee/rr/ol3/
export const MAAAMET_TMS_HYBRID = 'http://tiles.maaamet.ee/tm/s/1.0.0/hybriid/{z}/{x}/{-y}.png';
export const MAAAMET_TMS_FOTO = 'http://tiles.maaamet.ee/tm/s/1.0.0/foto/{z}/{x}/{-y}.jpg';
export const DEFAULT_PROJECTION = 'EPSG:4326';
export const ESTONIA_PROJECTION = 'EPSG:3301';
export const ESTONIA_MIN_ZOOM = 4;
export const ESTONIA_EXTENT = [40500, 5993000, 1064500, 7017000];
export const ESTONIA_VIEW_EXTENT = [360228, 6375178, 743166, 6619913];
export const ESTONIA_CENTER = [516493.16, 6513417.97];
export const ESTONIA_RESOLUTIONS = [4000, 2000, 1000, 500, 250, 125, 62.5, 31.25, 15.625, 7.8125, 3.90625, 1.953125, 0.9765625, 0.48828125];

/* ZOOMS */
export const DEFAULT_ZOOM = 9;

/* GEOMETRY STYLES */
export const MIN_RESOLUTION_TO_SHOW_TEXT = 1;
export const GEOMETRY_TEXT_STYLE_FONT = '18px Calibri,sans-serif';
export const GEOMETRY_TEXT_STYLE_COLOR = 'white';
export const GEOMETRY_TEXT_STYLE_BORDER_COLOR = 'black';

export const BRIDGE_POLYGON_BORDER_COLOR = 'rgba(15, 196, 184, 0.6)';
export const BRIDGE_POLYGON_BORDER_WIDTH = 3;
export const BRIDGE_POLYGON_BACKGROUND_COLOR = 'rgba(15, 196, 184, 0.2)';

export const ANALYSIS_POINT_RADIUS = 8;
export const ANALYSIS_POINT_BORDER_COLOR = 'white';
export const ANALYSIS_POINT_ACTIVE_BORDER_COLOR = '#266155';
export const ANALYSIS_POINT_BORDER_WIDTH = 2;

export const ANALYSIS_POINT_COLOR_HSL_START = 0; // red
export const ANALYSIS_POINT_COLOR_HSL_STOP = 255; // blue

export const DRAWN_AVERAGE_AREA_OPACITY = 0.6;
export const DRAWN_AVERAGE_NUMBER_FRACTION = 2;

/* Average value (on graph) fraction */
export const AVERAGE_VALUE_FRACTION = 2; // .toFixed(2)

/* LEGEND CONSTANTS */
export const LEGEND_DISPLACEMENT_RANGE_MIN = -15;
export const LEGEND_DISPLACEMENT_RANGE_MAX = 15;
export const LEGEND_VELOCITY_RANGE_MIN = -5;
export const LEGEND_VELOCITY_RANGE_MAX = 5;

/* ORBIT CONSTANTS */
export const ASC_ORBIT_LABEL = 'ASC';
export const ASC_ORBIT_NUMBER = 160;
export const ASC_ORBIT_ANGLE = 76;

export const DESC_ORBIT_LABEL = 'DESC';
export const DESC_ORBIT_NUMBER = 80;
export const DESC_ORBIT_ANGLE = 284;

export const ORBIT_NUMBER_MAP = {
  [DESC_ORBIT_NUMBER]: {
    label: DESC_ORBIT_LABEL,
    number: DESC_ORBIT_NUMBER,
    angle: DESC_ORBIT_ANGLE,
  },
  [ASC_ORBIT_NUMBER]: {
    label: ASC_ORBIT_LABEL,
    number: ASC_ORBIT_NUMBER,
    angle: ASC_ORBIT_ANGLE,
  }
};

/* POPUP */
export const POINT_POPUP_OFFSET_XY = [-100, -120];

/* POINTS EVENTS */
export const POINT_EVENT_CLICK = 'click';
export const POINT_EVENT_HOVER = 'pointermove';

/* DRAW EVENTS */
export const DRAW_END_EVENT = 'drawend';

/* LINE GRAPH */
export const AVERAGE_LINE_COLOR = 'gray';
export const REGRESSION_LINE_COLOR = 'rgba(53, 107, 98, 0.7)';

/* TOOLTIPS */
export const TOOLTIP_SELECT_BRIDGE = '1. Vali sild';
export const TOOLTIP_SELECT_ANALYSIS = '2. Vali analüüs';
export const TOOLTIP_START_USING_IT = '3. Alusta tööd';
export const TOOLTIP_YOU_CAN_SCROLL = 'Skaalat on võimalik scrollida.';
export const TOOLTIP_DRAW_ACTIVATED = 'Keskmise leidmine / punktide filtreerimine on sisse lülitatud.';
export const TOOLTIP_DRAW_DISACTIVATED = 'Keskmise leidmine / punktide filtreerimine on välja lülitatud';
