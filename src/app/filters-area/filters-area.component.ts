import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FeatureFilters } from '../domain/FeatureFilters';
import { DataService } from '../services/data.service';
import { Message, SelectItem } from 'primeng/primeng';
import { Bridge } from '../domain/Bridge';
import { MapService } from '../services/map.service';
import { BridgeAnalysis } from '../domain/BridgeAnalysis';
import { Legend } from '../domain/Legend';
import { FeatureType } from '../domain/FeatureType';
import { DEFAULT_ZOOM, TOOLTIP_SELECT_BRIDGE, TOOLTIP_SELECT_ANALYSIS, TOOLTIP_START_USING_IT } from '../app.constants';

import * as _ from 'lodash';

@Component({
  selector: 'app-filters-area',
  templateUrl: './filters-area.component.html',
  styleUrls: ['./filters-area.component.css', './filters-area.select.css'],
  encapsulation: ViewEncapsulation.None
})
export class FiltersAreaComponent implements OnInit {

  @Input()
  public filters: FeatureFilters;

  @Input()
  msgs: Message[];

  @Output()
  onReset = new EventEmitter();

  private static toSelectItems(arr: any[]): SelectItem[] {
    return _.map(arr, item => item.toSelectItem());
  }

  constructor(private dataService: DataService, private mapService: MapService) { }

  ngOnInit() {
    this.dataService.getBridges().subscribe(bridges => this.updateBridgeList(bridges));
    this.msgs.push({severity: 'info', summary: 'Abi', detail: TOOLTIP_SELECT_BRIDGE });
  }

  bridgeChange(bridge: Bridge): void {
    this.filters.reset();
    this.onReset.emit();

    this.mapService.clearAllFeatures();
    this.mapService.addFeature(FeatureType.BRIDGE, bridge.featureReference);
    this.mapService.addFeature(FeatureType.ARTIFICIAL_SCATTERER, bridge.getArtificialScatterersAsFeatures());

    this.mapService.fitExtent(bridge.geometry.getExtent(), DEFAULT_ZOOM);
    this.dataService.getBridgeAnalysis(bridge.id)
        .subscribe(bridgeAnalysis => this.updateBridgeAnalysisList(bridgeAnalysis));
    this.msgs.push({severity: 'info', summary: 'Abi', detail: TOOLTIP_SELECT_ANALYSIS});
  }

  analysisChange(bridgeAnalysis: BridgeAnalysis): void {
    this.mapService.clearPoints();
    this.mapService.clearDrawnFeatures();
    this.onReset.emit();

    this.dataService.getAnalysisPoints(bridgeAnalysis.id).subscribe(points => {
      bridgeAnalysis.setPoints(points);
      this.mapService.addFeature(FeatureType.POINT, bridgeAnalysis.getPointsAsFeatures());
    });
    this.msgs.push({severity: 'info', summary:'Abi', detail: TOOLTIP_START_USING_IT});
  }

  legendChange(newLegend: Legend): void {
    const { selectedAnalysis } = this.filters;
    if (selectedAnalysis) {
      selectedAnalysis.legend = newLegend;
      this.mapService.updateLegendFeatures();
    }
  }

  private updateBridgeAnalysisList(bridgeAnalysis: BridgeAnalysis[]): void {
    _.each(bridgeAnalysis, analysis => {
      analysis.bridge = this.filters.selectedBridge;
      analysis.legend = this.filters.selectedLegend;
    });
    const sortedList = _.reverse(_.sortBy(bridgeAnalysis, analysis => analysis.lastImageDate));
    this.filters.analysis = FiltersAreaComponent.toSelectItems(sortedList);
  }

  private updateBridgeList(bridges: Bridge[]): void {
    this.filters.bridges = FiltersAreaComponent.toSelectItems(bridges);
  }

}
