import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersAreaComponent } from './filters-area.component';
import { DataServiceMock } from '../services/test/data.service.mock';
import { DataService } from '../services/data.service';
import { MapService } from '../services/map.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FeatureFilters } from '../domain/FeatureFilters';
import { DisplacementLegend } from '../domain/Legends';

describe('FiltersAreaComponent', () => {
  let component: FiltersAreaComponent;
  let fixture: ComponentFixture<FiltersAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltersAreaComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [MapService, {provide: DataService, useValue: new DataServiceMock()}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersAreaComponent);
    component = fixture.componentInstance;
    component.msgs = [];
    component.filters = new FeatureFilters();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load bridges to dropdown with displacement legend initially', () => {
    expect(component.filters).toBeDefined();
    expect(component.filters.bridges.length).toBe(2);
    expect(component.filters.analysis).not.toBeDefined();
    expect(component.filters.selectedBridge).not.toBeDefined();
    expect(component.filters.selectedLegend.value).toBe(DisplacementLegend.value);
  });

  it('should change bridge and query analysis', () => {
    component.bridgeChange(component.filters.bridges[0].value);
    fixture.detectChanges();

    expect(component.filters.selectedAnalysis).not.toBeTruthy();
    expect(component.filters.analysis.length).toBe(2);
  });

  it('should change analysis and query points', () => {
    component.bridgeChange(component.filters.bridges[0].value);
    fixture.detectChanges();

    component.analysisChange(component.filters.analysis[0].value);
    fixture.detectChanges();

    expect(component.filters.analysis[0].value.points.length).toBe(2);
  });

});
