import { geom, style, Feature, proj } from 'openlayers';
import {
  ANALYSIS_POINT_BORDER_COLOR,
  ANALYSIS_POINT_BORDER_WIDTH,
  ANALYSIS_POINT_RADIUS,
  ANALYSIS_POINT_COLOR_HSL_START,
  ANALYSIS_POINT_COLOR_HSL_STOP, ANALYSIS_POINT_ACTIVE_BORDER_COLOR,
} from '../app.constants';
import { AbstractFeature } from './AbstractFeature';
import { BridgeAnalysis } from './BridgeAnalysis';

export class AnalysisPoint extends AbstractFeature {

  id: number;
  location: any;
  description: string;

  coherence: number;
  cumulativeDisplacement: number;
  height: number;
  heightWrt: number;
  velocity: number;
  sigmaHeight: number;
  sigmaVelocity: number;

  active = false;

  analysis: BridgeAnalysis;
  pointReference: AnalysisPoint;

  public constructor(data: any) {
    super(data);

    this.id = data.id;
    this.location = data.location;
    this.description = data.pointName;
    this.coherence = data.coherence;
    this.cumulativeDisplacement = data.cumulativeDisplacement;
    this.height = data.height;
    this.heightWrt = data.heightWrt;
    this.velocity = data.velocity;
    this.sigmaHeight = data.sigmaHeight;
    this.sigmaVelocity = data.sigmaVelocity;
    this.pointReference = this;

    this.initFeature();
  }

  protected getStyleFunction(): any {
    return (feature, resolution) => {
      const isActive = this.active;
      return [new style.Style({
        image: new style.Circle({
          radius: ANALYSIS_POINT_RADIUS,
          stroke: new style.Stroke({
            color: isActive
              ? ANALYSIS_POINT_ACTIVE_BORDER_COLOR
              : ANALYSIS_POINT_BORDER_COLOR,
            width: ANALYSIS_POINT_BORDER_WIDTH,
          }),
          fill: new style.Fill({
            color: this.generateHSLValue()
          })
        })
      }),
      new style.Style({
        image: new style.Icon({
          anchor: [0.5, 0.5],
          size: [26, 26],
          rotation: Math.PI * (feature.get('pointReference').analysis.orbit.angle / 180),
          src: '../../assets/arrow.png'
        })
      })];
    };
  }

  getScalePercentage(overrideValue?: number): number {
    const legend = this.analysis.legend;
    const min = legend.scaleMin;
    const max = legend.scaleMax;
    const value = overrideValue || this[legend.value];
    const scaleIntervalLength = max - min;
    const valueIntervalLength = value - min;
    const percentage = valueIntervalLength / scaleIntervalLength;
    return percentage > 1 ? 1 : (percentage < 0 ? 0 : percentage);
  }

  generateHSLValue(value?: number, transparency?: number): string {
    const percentage = this.getScalePercentage(value);
    const hue = (percentage * (ANALYSIS_POINT_COLOR_HSL_STOP - ANALYSIS_POINT_COLOR_HSL_START)) + ANALYSIS_POINT_COLOR_HSL_START;
    return `hsla(${hue}, 100%, 50%, ${transparency || 1})`;
  }

}
