export interface Legend {
  readonly label: string;
  readonly value: string;

  scaleMin: number;
  scaleMax: number;
}
