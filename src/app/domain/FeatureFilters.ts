import { SelectItem } from 'primeng/primeng';
import { Bridge } from './Bridge';
import { Legends } from './Legends';
import { Legend } from './Legend';
import { BridgeAnalysis } from './BridgeAnalysis';

import * as _ from 'lodash';

export class FeatureFilters {

  /* Bridges filter */
  public bridges: SelectItem[];
  public selectedBridge: Bridge;

  /* Analysis filter */
  public analysis: SelectItem[];
  public selectedAnalysis: BridgeAnalysis;

  /* Legend based on value filter */
  public legends: SelectItem[];
  public selectedLegend: Legend;

  constructor() {
    this.reset();
  }

  reset(): void {
    this.selectedAnalysis = null;
    this.legends = _.cloneDeep(Legends);
    this.selectedLegend = this.legends[0].value;
  }

}
