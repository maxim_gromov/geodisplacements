import { SelectItem } from 'primeng/components/common/api';
import { geom, style, Feature, proj } from 'openlayers';
import { AbstractFeature } from './AbstractFeature';

import {
  BRIDGE_POLYGON_BACKGROUND_COLOR,
  BRIDGE_POLYGON_BORDER_COLOR,
  BRIDGE_POLYGON_BORDER_WIDTH,
} from '../app.constants';
import { ArtificialScatterer } from './ArtificialScatterer';

import * as _ from 'lodash';

export class Bridge extends AbstractFeature {

  id: number;
  name: string;
  artificialScatterers: ArtificialScatterer[] = [];

  public constructor(data: any) {
    super(data);

    this.id = data.id;
    this.name = data.name;

    const scatterers = data.artificialScatterers;
    if (scatterers && scatterers.length > 0) {
      scatterers.forEach(scatterer => this.artificialScatterers.push(new ArtificialScatterer(scatterer)));
    }

    this.initFeature();
  }

  toSelectItem(): SelectItem {
    return {
      label: this.name,
      value: this,
    };
  }

  getArtificialScatterersAsFeatures(): Feature[] {
    return _.map(this.artificialScatterers, scatterer => scatterer.featureReference);
  }

  protected getStyleFunction(): any {
    return () => {
      return [new style.Style({
        stroke: new style.Stroke({
          color: BRIDGE_POLYGON_BORDER_COLOR,
          width: BRIDGE_POLYGON_BORDER_WIDTH
        }),
        fill: new style.Fill({
          color: BRIDGE_POLYGON_BACKGROUND_COLOR
        })
      })];
    };
  }

}
