import { AbstractFeature } from './AbstractFeature';
import { geom, style, Feature, proj } from 'openlayers';

export class ArtificialScatterer extends AbstractFeature {

  constructor(data: any) {
    super(data);
    this.initFeature();
  }

  protected getStyleFunction() {
    return () => {
      return [new style.Style({
        image: new style.Circle({
          radius: 5,
          stroke: new style.Stroke({
            color: 'black',
            width: 3,
          }),
          fill: new style.Fill({
            color: 'white'
          })
        })
      })];
    };
  }

}
