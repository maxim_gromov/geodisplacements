import { DEFAULT_PROJECTION, ESTONIA_PROJECTION } from '../app.constants';
import { geom, Feature } from 'openlayers';

export abstract class AbstractFeature {

  geometry: geom.Geometry;
  featureReference: Feature;

  constructor(data: {location: {type: string, coordinates: any[]}}) {
    this.initGeometry(data.location);
  }

  protected initGeometry(location: {type: string, coordinates: any[]}) {
    this.geometry = new geom[location.type](location.coordinates).transform(DEFAULT_PROJECTION, ESTONIA_PROJECTION);
  }

  protected abstract getStyleFunction(): any;

  protected initFeature(): void {
    const feature = new Feature(this);
    feature.setStyle(this.getStyleFunction());
    this.featureReference = feature;

  }
}
