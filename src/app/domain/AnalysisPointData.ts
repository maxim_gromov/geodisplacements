
export class AnalysisPointData {

  id: number;
  pointId: number;
  imageDate: string;
  displacement: number;

  public constructor(data: any) {
    this.id = data.id;
    this.pointId = data.outputPoint.id;
    this.imageDate = data.imageDate;
    this.displacement = data.displacement;
  }

}
