import { SelectItem } from 'primeng/components/common/api';
import { DatePipe } from '@angular/common';
import { DEFAULT_DATE_FORMAT, DEFAULT_LOCALE, ORBIT_NUMBER_MAP } from '../app.constants';
import { AnalysisPoint } from './AnalysisPoint';
import { Legend } from './Legend';

import { DisplacementLegend } from './Legends';
import { Bridge } from './Bridge';
import { Feature } from 'openlayers';

import * as _ from 'lodash';

export class BridgeAnalysis {

  id: number;
  label: string;
  orbit: { label: string, number: number, angle: number };
  legend: Legend;
  analysisDate: Date;
  firstImageDate: Date;
  lastImageDate: Date;

  bridge: Bridge;
  points: AnalysisPoint[];

  public constructor(data: any) {
    this.id = data.id;
    this.analysisDate = new Date(data.analysisDate);
    this.firstImageDate = new Date(data.firstImageDate);
    this.lastImageDate = new Date(data.lastImageDate);
    this.orbit = ORBIT_NUMBER_MAP[data.relativeOrbitNumber];
    this.legend = DisplacementLegend;
    this.generateLabel();
  }

  toSelectItem(): SelectItem {
    return {
      label: this.label,
      value: this,
    };
  }

  setPoints(points: AnalysisPoint[]): void {
    _.each(points, point => point.analysis = this);
    this.points = points;
  }

  updatePointsActive(activePoint: AnalysisPoint): void {
    _.each(this.points, (point, index) => {
      point.active = point === activePoint;
      point.featureReference.changed();
    });
  }

  getPointsAsFeatures(): Feature[] {
    return _.map(this.points, point => point.featureReference);
  }

  private generateLabel(): void {
    const datePipe = new DatePipe(DEFAULT_LOCALE);
    const firstImageDate = datePipe.transform(this.firstImageDate, DEFAULT_DATE_FORMAT);
    const lastImageDate = datePipe.transform(this.lastImageDate, DEFAULT_DATE_FORMAT);
    this.label = `${firstImageDate} - ${lastImageDate}`;
  }

}
