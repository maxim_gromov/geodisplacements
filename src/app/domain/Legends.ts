import { SelectItem } from 'primeng/components/common/api';
import { Legend } from './Legend';
import {
  LEGEND_DISPLACEMENT_RANGE_MAX,
  LEGEND_DISPLACEMENT_RANGE_MIN,
  LEGEND_VELOCITY_RANGE_MAX,
  LEGEND_VELOCITY_RANGE_MIN,
} from '../app.constants';

export const DisplacementLegend: Legend = {
  label: 'Displacement',
  value: 'cumulativeDisplacement',
  scaleMin: LEGEND_DISPLACEMENT_RANGE_MIN,
  scaleMax: LEGEND_DISPLACEMENT_RANGE_MAX,
};
export const VelocityLegend: Legend = {
  label: 'Velocity',
  value: 'velocity',
  scaleMin: LEGEND_VELOCITY_RANGE_MIN,
  scaleMax: LEGEND_VELOCITY_RANGE_MAX,
};

export const Legends: SelectItem[] = [
  {label: DisplacementLegend.label, value: DisplacementLegend},
  {label: VelocityLegend.label, value: VelocityLegend},
];
