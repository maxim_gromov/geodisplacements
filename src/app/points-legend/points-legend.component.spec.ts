import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsLegendComponent } from './points-legend.component';
import { MapService } from '../services/map.service';
import { DisplacementLegend } from '../domain/Legends';

describe('PointsLegendComponent', () => {
  let component: PointsLegendComponent;
  let fixture: ComponentFixture<PointsLegendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsLegendComponent ],
      providers: [MapService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsLegendComponent);
    component = fixture.componentInstance;
    component.legend = DisplacementLegend;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
