import { Component, Input, OnInit } from '@angular/core';
import { Legend } from '../domain/Legend';
import { MapService } from '../services/map.service';
import { AnalysisPoint } from '../domain/AnalysisPoint';

import { POINT_EVENT_HOVER, TOOLTIP_YOU_CAN_SCROLL } from '../app.constants';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'app-points-legend',
  templateUrl: './points-legend.component.html',
  styleUrls: ['./points-legend.component.css']
})
export class PointsLegendComponent implements OnInit {

  @Input()
  legend: Legend;

  @Input()
  msgs: Message[];

  hoveredPoint: AnalysisPoint;

  private isTooltipShown = false;

  constructor(private mapService: MapService) { }

  ngOnInit() {
    this.mapService.createPointPopup();
    this.onFeatureHoverListener();
  }

  increaseScale(): void {
    this.legend.scaleMin--;
    this.legend.scaleMax++;
    this.mapService.updateLegendFeatures();
  }

  decreaseScale(): void {
    this.legend.scaleMin++;
    this.legend.scaleMax--;
    this.mapService.updateLegendFeatures();
  }

  onFeatureHoverListener(): void {
    this.mapService.onPointEvent(POINT_EVENT_HOVER, (point, coordinate) => {
      const popup = this.mapService.pointPopup;
      this.hoveredPoint = point;
      if (this.hoveredPoint) {
        document.body.style.cursor = 'pointer';
        popup.setPosition(coordinate);
      } else {
        document.body.style.cursor = 'default';
      }
    });
  }

  mouseMove(): void {
    if (!this.isTooltipShown) {
      this.msgs.push({severity: 'info', summary: 'Abi', detail: TOOLTIP_YOU_CAN_SCROLL });
      this.isTooltipShown = true;
    }
  }

}
